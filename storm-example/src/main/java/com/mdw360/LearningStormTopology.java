package com.mdw360;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;

/**
 * Created by derekmarley on 14-12-24.
 */
public class LearningStormTopology {

    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {

        // create an instance of TopologyBuilder class
        TopologyBuilder builder = new TopologyBuilder();

        // set the spout class
        builder.setSpout("LearningStormSpout",
                new LearningStormSpout(), 2);

        // set the bolt class
        builder.setBolt("LearningStormBolt",
                new LearningStormBolt(), 4).shuffleGrouping("LearningStormSpout");

        Config conf = new Config();
        conf.setDebug(true);
        conf.setNumWorkers(3);


        // create an instance of LocalCluster class for executing topology in local mode.
        LocalCluster cluster = new LocalCluster();

        // LearningStormTopolgy is the name of submitted topology.
        cluster.submitTopology("LearningStormToplogy", conf, builder.createTopology());

        try {
            Thread.sleep(10000);
        } catch (Exception exception) {
            System.out.println("Thread interrupted exception : " + exception);
        }

        // kill the LearningStormTopology
        cluster.killTopology("LearningStormToplogy");
        // shutdown the storm test cluster
        cluster.shutdown();
    }
}
